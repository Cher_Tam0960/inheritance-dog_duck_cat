/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.inheritance;

/**
 *
 * @author kitti
 */
public class Testanimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        NextLine();

        Dog dang = new Dog("Dang", "White & Black");
        dang.speak();
        dang.walk();
        NextLine();
        
        Dog Tor = new Dog("Tor", "Orange");
        Tor.speak();
        Tor.walk();
        NextLine();
        
        Dog Mome = new Dog("Mome", "White & Black");
        Mome.speak();
        Mome.walk();
        NextLine();
        
        Dog Bat = new Dog("Bat", "White & Black");
        Bat.speak();
        Bat.walk();
        NextLine();

        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        NextLine();

        Duck som = new Duck("Zom", "Yellow");
        som.speak();
        som.walk();
        som.fly();
        NextLine();
        
        Duck Gabgab = new Duck("Gabgab", "Yellow");
        Gabgab.speak();
        Gabgab.walk();
        Gabgab.fly();
        NextLine();

        System.out.println("Som is an Animal : " + (som instanceof Animal));
        System.out.println("Som is an Duck : " + (som instanceof Duck));
        System.out.println("Som is an Object : " + (som instanceof Object));
        System.out.println("Animal is Dog : " + (animal instanceof Dog));
        System.out.println("Animal is Animal : " + (animal instanceof Animal));
        
        
        Animal ani1 = null;
        Animal ani2 = null;
        ani1 = som;
        ani2 = zero;

        System.out.println("Ani1 : Som is a Duck " + (som instanceof Duck));
        NextLine();
        
        Animal[] animals = {dang, Tor, Mome, Bat, zero, som, Gabgab};
        for (int i = 0; i < animals.length; i++) {
            System.out.println("***************");
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck) {
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println("***************");
        }
    }

    private static void NextLine() {
        System.out.println("");
    }
}
